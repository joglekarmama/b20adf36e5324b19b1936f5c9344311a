import Phaser from 'phaser';
import config from 'visual-config-exposer';

let ball, ends, hoop, board, collider;
let start_pos, end_pos;
let scoreSound, backboardSound, whooshSound, failSound, spawnSound;
let counter = 0;
let score = 0,
  scoreText,
  timeText;

class Game extends Phaser.Scene {
  // backgroundScene;

  constructor() {
    super('playGame');
    this.GAME_WIDTH = 740;
    this.GAME_HEIGHT = 900;
    this.pointer = null;
    this.shoot = null;
  }

  init(data) {
    score = data.score || 0;
  }

  create() {
    const width = this.scale.gameSize.width;
    const height = this.scale.gameSize.height;

    scoreSound = this.sound.add('score', {
      volume: config.preGame.musicVolume,
    });
    // backboardSound = this.sound.add('backboard',{
    //   volume: config.preGame.musicVolume,
    // });
    whooshSound = this.sound.add('whoosh', {
      volume: config.preGame.musicVolume,
    });
    failSound = this.sound.add('fail', {
      volume: config.preGame.musicVolume,
    });
    spawnSound = this.sound.add('spawn', {
      volume: config.preGame.musicVolume,
    });

    this.parent = new Phaser.Structs.Size(width, height);
    if (window.innerWidth < 800 || window.innerWidth === undefined) {
      this.sizer = new Phaser.Structs.Size(
        this.GAME_WIDTH,
        this.GAME_HEIGHT,
        Phaser.Structs.Size.ENVELOP,
        this.parent
      );
    } else if (window.innerWidth >= 800) {
      this.sizer = new Phaser.Structs.Size(
        this.GAME_WIDTH,
        this.GAME_HEIGHT,
        Phaser.Structs.Size.FIT,
        this.parent
      );
    }

    this.parent.setSize(width, height);
    this.sizer.setSize(width, height);

    this.backgroundScene = this.add
      .image(0, 0, 'background')
      .setOrigin(0, 0)
      .setScale(5);

    this.backgroundScene.depth = 0;

    this.physics.world.setBounds(0, 0, this.GAME_WIDTH, this.GAME_HEIGHT);

    this.ground = this.physics.add.staticGroup();

    this.ground
      .create(370, 900, 'ground')
      .setDisplaySize(740, 600)
      .refreshBody();

    this.platform = this.physics.add.staticGroup();
    this.platform
      .create(this.GAME_WIDTH / 2, this.GAME_HEIGHT - 50, 'platform')
      .setDisplaySize(50, 50)
      .refreshBody();
    this.platform.scaleXY(
      +config.preGame.platformScale,
      +config.preGame.platformScale
    );
    this.platform.setOrigin(1, 1);
    // this.platform.setSize(100, 10);

    board = this.add.image(370, 200, 'board');
    board.setScale(0.6);
    board.depth = 1;

    this.advertisement = this.add.image(370, 147, 'add');
    this.advertisement.setScale(config.preGame.addScale).setDepth(1);

    hoop = this.physics.add.staticImage(370, 202, 'hoop').setScale(0.9);
    hoop.depth = 2;

    ends = this.physics.add.staticGroup();
    ends.create(283, 203, 'side').setCircle(2);
    ends.create(457, 203, 'side').setCircle(2);

    scoreText = this.add.text(150, 10, 'score: 0', {
      fontSize: '32px',
      fill: config.game.textColor,
    });

    timeText = this.add.text(370, 10, `Time Left: ${config.game.timer}`, {
      fontSize: '32px',
      fill: config.game.textColor,
    });

    this.createBall();
    this.physics.add.overlap(ball, hoop);

    if (config.game.enableTimer) {
      this.timeLeft = config.game.timer;
      this.gameTimer = this.time.addEvent({
        delay: 1000,
        callback: function () {
          this.timeLeft--;
          timeText.setText(`Time Left:${this.timeLeft}`);
          if (config.game.enableTimer) {
            if (score >= config.game.winScore && this.timeLeft <= 0) {
              this.scene.start('endGame', { score, message: 'You Win' });
            } else if (score < config.game.winScore && this.timeLeft <= 0) {
              this.scene.start('endGame', { score, message: 'You Lose' });
            }
          } else {
            if (score >= config.game.winScore) {
              this.scene.start('endGame', { score, message: 'You Win' });
            } else {
              this.scene.start('endGame', { score, message: 'You Lose' });
            }
          }
        },
        callbackScope: this,
        loop: true,
      });
    }

    this.updateCamera();
    this.scale.on('resize', this.resize, this);
  }

  update() {
    if (!config.game.enableTimer) {
      if (score >= config.game.winScore) {
        this.scene.start('endGame', { score, message: 'You Win' });
      } else {
        this.scene.start('endGame', { score, message: 'You Lose' });
      }
    }

    if (ball.body.touching.none && ball.launched && ball.body.velocity.y > 0) {
      this.physics.add.collider(ball, ends);
    }

    if (
      ball.body.y > hoop.body.y &&
      ball.launched &&
      ball.body.velocity.y > 0
    ) {
      ball.isBelowHoop = true;
    }

    if (ball.launched && ball.body.velocity.y > 0) {
      ball.depth = 1;
    }

    if (ball && ball.body.velocity.y > 0 && ball.launched && ball.isBelowHoop) {
      if (
        ball.body.x > ends.children.entries[0].x &&
        ball.body.x < ends.children.entries[1].x
      ) {
        counter += 1;
        if (counter === 1) {
          scoreSound.play();
          score += 10;
          scoreText.setText('Score:' + score);
        }
      }
    }

    if (ball && ball.body.y > 1200) {
      ball.disableBody(false, false);
      if (counter === 0) {
        failSound.play();
      }
      this.createBall();
    }

    if (score >= 10) {
    }
  }

  createBall() {
    // ball = this.add.image(370, this.GAME_HEIGHT, 'ball');
    ball = this.physics.add.image(370, this.GAME_HEIGHT - 132, 'ball');
    ball.setScale(+config.preGame.ballScale);
    // ball.body.allowGravity = true;
    ball.setBounce(0.8);
    ball.depth = 3;
    ball.setCollideWorldBounds(true);
    ball.setCircle(250);
    ball.launched = false;
    ball.isBelowHoop = true;
    // this.collider = this.physics.add.collider(ball, this.platform);
    collider = this.physics.world.addCollider(ball, this.platform);
    spawnSound.play();

    counter = 0;

    this.pointer = ball
      .setInteractive({ draggable: true })
      .on('dragstart', hold)
      .on('dragend', release);

    this.input.setDraggable(ball);
    this.input.on('dragend', this.addTween.bind(this));
    this.input.on('dragend', this.removeCollider.bind(this));
  }

  removeCollider() {
    this.physics.world.removeCollider(collider);
  }

  addTween() {
    if (ball.launched) {
      this.tweens.add({
        targets: ball,
        rotation: '45',
        duration: 4000,
      });

      this.tweens.add({
        targets: ball,
        scaleX: config.preGame.ballThrowScale,
        scaleY: config.preGame.ballThrowScale,
        duration: 500,
      });
    }
  }

  resize(gameSize) {
    const width = gameSize.width;
    const height = gameSize.height;

    this.parent.setSize(width, height);
    this.sizer.setSize(width, height);

    this.updateCamera();
  }

  updateCamera() {
    const camera = this.cameras.main;

    const x = Math.ceil((this.parent.width - this.sizer.width) * 0.5);
    const y = 0;
    const scaleX = this.sizer.width / this.GAME_WIDTH;
    const scaleY = this.sizer.height / this.GAME_HEIGHT;

    camera.setViewport(x, y, this.sizer.width, this.sizer.height);
    camera.setZoom(Math.max(scaleX, scaleY));
    camera.centerOn(this.GAME_WIDTH / 2, this.GAME_HEIGHT / 2);
  }

  getZoom() {
    return this.cameras.main.zoom;
  }
}

export default Game;

function hold(pointer, X, Y, event) {
  start_pos = [pointer.x, pointer.y];
}

function release(pointer, X, Y, event) {
  let x_dir;
  end_pos = [pointer.x, pointer.y];
  if (end_pos[1] < start_pos[1]) {
    const slope = [end_pos[0] - start_pos[0], end_pos[1] - start_pos[1]];
    x_dir = (-2300 * slope[0]) / slope[1];
  }
  shoot(x_dir);
}

function shoot(x_dir) {
  if (x_dir) {
    ball.body.velocity.x = x_dir / 2.3;
    ball.body.velocity.y = -1350;
    // ball.setAngle(x_dir );
    ball.setAccelerationY(1000);
    ball.launched = true;
    ball.isBelowHoop = false;
    ball.setCollideWorldBounds(false);
    whooshSound.play();
  }
}
